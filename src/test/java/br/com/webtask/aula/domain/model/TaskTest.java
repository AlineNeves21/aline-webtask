/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.model;

import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import java.time.LocalDate;

public class TaskTest {
    
    public TaskTest () {
        
    }
    
    @Test
    public void testSomeMethod() {
        //cenario
        int a = 3;
        int b = 5;
        //executar
        int c = a + b;
        //verificar
        Assertions.assertEquals(8,c);
    }
    
    @Test
    @DisplayName("Verificar se uma task atrasada fica com o status em Atraso")
    public void verificarSeUmaTaskAtrasadaEstaNesseStatus (){
        //cenario
        Task t = new Task(1l,"estudar",LocalDate.now().minusDays((2)), null, null);
        //executar
        EStatus status = t.getStatus();      
        //verificar
        Assertions.assertEquals(EStatus.ATRASADO, status);
    }
    
    @Test
    @DisplayName("Testar o status novo de uma task")
    public void statusTaskNovo (){
        //cenario
        Task t = new Task(1l,"estudar",LocalDate.now(), null, null);
        //executar
        EStatus status = t.getStatus();      
        //verificar
        Assertions.assertEquals(EStatus.NOVO, status);
    }
    
    @Test
    @DisplayName("Testar o status concluído de uma task, porém atrasada")
    public void statusTaskConcluidoAtrasado(){
        //cenario
        Task t = new Task(1l,"estudar",LocalDate.now(), LocalDate.now().plusDays(2), null);
        //executar
        EStatus status = t.getStatus();      
        //verificar
        Assertions.assertEquals(EStatus.CONCLUIDO_ATRASADO, status);
    }
    
    @Test
    @DisplayName("Testar o status concluído dentro do prazo de uma task")
    public void statusTaskConcluidoPrazo(){
        //cenario
        Task t = new Task(1l,"estudar",LocalDate.now(), LocalDate.now(), null);
        //executar
        EStatus status = t.getStatus();      
        //verificar
        Assertions.assertEquals(EStatus.CONCLUIDO_PRAZO, status);
    }
    
    @Test
    @DisplayName("Testar o método IsFinish")
    public void testarIsFinish() {
        //cenario
        Task task = new Task();
        task.setPlannedDate(LocalDate.now().plusDays(2));
        //executar
        task.setFinishedDate(LocalDate.now());
        //verificar
        Assertions.assertEquals(true,task.isFinish());
    }
    
    @Test
    @DisplayName("Testar o método IsLate ")
    public void testarIsLate() {   
        //cenario
        Task task = new Task();
        task.setPlannedDate(LocalDate.now().minusDays(2));
        //executar
        task.setFinishedDate(LocalDate.now());
        //verificar
        Assertions.assertEquals(true,task.isLate());
       
    }
}
